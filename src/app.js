import React, { Component } from "react";
import Card from "./components/card";
import axios from "axios";
//axios.defaults.baseURL = process.env.api_endpoint;
class app extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: { classic: 0, standout: 0, premium: 0 },
      total: 0
    };
  }
  orderChangeHandler = (product, amount) => {
    let p = this.state.products;
    p[product] = amount;
    this.setState({ products: p });
  };
  checkoutHandler = async () => {
    const obj = {
      customer: "Nike",
      products: this.state.products
    };
    const { data } = await axios.post(
      "http://52.77.44.192:3001/ads/checkout",
      obj
    );
    this.setState({ total: data.price });
  };
  render() {
    return (
      <div className="container">
        <div className="col-4 m-3">
          <Card
            products={this.state.products}
            total={this.state.total}
            onOrderChange={this.orderChangeHandler}
            onCheckout={this.checkoutHandler}
          />
        </div>
      </div>
    );
  }
}

export default app;
