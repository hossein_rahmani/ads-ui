import React from "react";

const Card = props => {
  return (
    <div className="card">
      <div className="card-header">Order Summary</div>
      <div className="card-body">
        <table className="table table-hover">
          <tbody>
            <tr>
              <td>Classic</td>
              <td>
                <input
                  type="number"
                  className="form-control"
                  name="txtClassic"
                  min="0"
                  max="10"
                  value={props.products.classic}
                  onChange={e => props.onOrderChange("classic", e.target.value)}
                />
              </td>
            </tr>
            <tr>
              <td>Standout</td>
              <td>
                <input
                  type="number"
                  className="form-control"
                  name="txtStandout"
                  min="0"
                  max="10"
                  value={props.products.standout}
                  onChange={e =>
                    props.onOrderChange("standout", e.target.value)
                  }
                />
              </td>
            </tr>
            <tr>
              <td>Premium</td>
              <td>
                <input
                  type="number"
                  className="form-control"
                  name="txtPremium"
                  min="0"
                  max="10"
                  value={props.products.premium}
                  onChange={e => props.onOrderChange("premium", e.target.value)}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <input
          type="button"
          className="btn btn-primary"
          value="Check out"
          onClick={props.onCheckout}
        />
      </div>
      <div className="card-footer">Total : {props.total}</div>
    </div>
  );
};

export default Card;
